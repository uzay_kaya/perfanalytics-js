window.start = () => {

  const HEADERS = {
    "Content-Type": "application/json"
  };
  const POST = "POST";
  const API_URL = "http://157.230.238.12:5000";

  const body = [];
  const createdAt = new Date().getTime();

  if(typeof(PerformanceObserver)!=='undefined'){ //if browser is supporting
    const observer = new PerformanceObserver((list) => {
      for (const entry of list.getEntries()) {
        if(entry.name === 'first-contentful-paint'){
          const fcp = {
            url: window.location.href,
            analyticType: "FCP",
            time: entry.startTime / 1000,
            createdAt,
          }
          body.push(fcp)
        }
      }
    });
    observer.observe({entryTypes: ['paint']});
  }

  window.onload = function () {
    setTimeout(() => {
      const { performance } = window;
      const performanceTiming = performance.timing;
      const performanceEntryResources = performance.getEntriesByType("resource");
      const performanceEntryNavigations = performance.getEntriesByType("navigation");
      let ttfb, domLoad, windowLoad = null;

      const isPerformanceDataAvailable = () => performance && !!performance.getEntriesByType && !!performanceTiming;

      if (isPerformanceDataAvailable()) {
        ttfb = ((performanceTiming.responseStart - performanceTiming.requestStart) / 1000)
        domLoad = ((performanceTiming.domComplete - performanceTiming.domLoading) / 1000)
        windowLoad = ((performanceTiming.domContentLoadedEventEnd - performanceTiming.navigationStart) / 1000)
      } else {
        return
      }

      const performanceData = [
        {
          url: window.location.href,
          analyticType: "TTFB",
          time: ttfb,
          createdAt,
        },
        {
          url: window.location.href,
          analyticType: "DOM_LOAD",
          time: domLoad,
          createdAt,
        },
        {
          url: window.location.href,
          analyticType: "WINDOW_LOAD",
          time: windowLoad,
          createdAt,
        },
      ];

      body.push(...performanceData);

      const getNetworkTimings = (p) => {
        p.forEach((e) => {
          body.push({
            url: e.name,
            analyticType: e.initiatorType,
            time: e.duration.toFixed(2),
            startTime: e.startTime.toFixed(2),
            createdAt,
          });
        });
      };

      getNetworkTimings(performanceEntryResources);
      getNetworkTimings(performanceEntryNavigations);

      console.log(body)
      setTimeout(() => {
        fetch(API_URL, {
          method: POST,
          headers: HEADERS,
          body: JSON.stringify(body),
        }).then((data) => console.log(data));
      },1000)
    }, 0)
  };
}