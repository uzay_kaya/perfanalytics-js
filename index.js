const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const app = express();
const port = 3500

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.text());

// Cors
const cors = require('cors')
app.use(cors())

app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, './PerfAnalytics.js'));
});

app.listen(port, () => {
  console.log(`PerfAnalytics JS running... on ${port}`);
});

module.exports = app;